import React from 'react'
import './../App.css';

export default function Main() {
    return (
        <div>
            <div className="img-container">
                    <h2 className="text-center text-white title">Iglesia Adventista del Septimo Dia</h2>
                    <button className="icon"><i className="fas fa-place-of-worship"></i></button>
                    <a className="btn btn-light">
                        Iglesias Cerca
                    </a>
                    
            </div>
        </div>
    )
}
